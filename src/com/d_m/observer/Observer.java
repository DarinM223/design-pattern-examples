package com.d_m.observer;

/**
 * An Observer is an interface that has a function update() that gets called whenever the
 * data its observing changes
 */
public interface Observer {
    public void update(float temp, float humidity, float pressure);
}

package com.d_m.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * WeatherData is a class that has weather data that changes all the time and needs to notify other elements
 * whenever it changes, so it implements Subject
 */
public class WeatherData implements Subject {
    private List<Observer> observers;
    // private state that changes constantly
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        observers = new ArrayList<Observer>();
        temperature = 0;
        humidity = 0;
        pressure = 0;
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    @Override
    public void notifyObservers() {
        // call update on all observers
        for (Observer observer : observers) {
            observer.update(temperature, humidity, pressure);
        }
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0) {
            observers.remove(i);
        }
    }
}

package com.d_m.observer;

/**
 * A Subject is an interface that lets implementations add and remove Observers that
 * "subscribe" to listening to events that are sent when notifyObservers() is called
 */
public interface Subject {
    public void registerObserver(Observer o);
    public void removeObserver(Observer o);
    public void notifyObservers();
}

package com.d_m.observer;

/**
 * A display element manages a "display" for viewing stuff
 */
public interface DisplayElement {
    public void display();
}

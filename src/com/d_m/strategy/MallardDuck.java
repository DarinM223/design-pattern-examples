package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        // Mallard Duck has same defaults as normal duck
        super();
    }
}

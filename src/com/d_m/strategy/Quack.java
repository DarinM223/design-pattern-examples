package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Quaaaack!!");
    }
}

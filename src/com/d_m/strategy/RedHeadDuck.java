package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class RedHeadDuck extends Duck {
    public RedHeadDuck() {
        super();
        this.setQuackBehavior(new Squeak());
    }
}

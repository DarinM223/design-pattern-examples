package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class Duck {
    private FlyBehavior flyBehavior = null;
    private QuackBehavior quackBehavior = null;

    public Duck() {
        // set behavior defaults
        this.setFlyBehavior(new FlyWithWings());
        this.setQuackBehavior(new Quack());
    }

    public void performQuack() throws NoQuackException {
        if (this.quackBehavior != null) {
            this.quackBehavior.quack();
        } else {
            throw new NoQuackException();
        }
    }

    public void performFly() throws NoFlyException {
        if (this.flyBehavior != null) {
            this.flyBehavior.fly();
        } else {
            throw new NoFlyException();
        }
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    public void swim() {
        System.out.println("Swimming!");
    }

    public void display() {
        System.out.println("I'm a duck!");
    }

    // other duck methods
}

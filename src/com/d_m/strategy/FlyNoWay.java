package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        // Do nothing
    }
}

package com.d_m.strategy.test;

import com.d_m.strategy.DecoyDuck;
import com.d_m.strategy.Duck;
import org.junit.Test;

import static org.junit.Assert.*;

public class DecoyDuckTest extends DuckTest {
    private Duck decoyDuck = new DecoyDuck();

    @Test
    public void testPerformQuack() throws Exception {
        decoyDuck.performQuack();

        // should print FlyNoWay behavior
        assertEquals("", outContent.toString());
    }

    @Test
    public void testPerformFly() throws Exception {
        decoyDuck.performFly();

        // should print MuteQuack behavior
        assertEquals("", outContent.toString());
    }
}
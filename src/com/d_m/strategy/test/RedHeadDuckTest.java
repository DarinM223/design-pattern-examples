package com.d_m.strategy.test;

import com.d_m.strategy.Duck;
import com.d_m.strategy.RedHeadDuck;
import org.junit.Test;

import static org.junit.Assert.*;

public class RedHeadDuckTest extends DuckTest {
    private Duck duck = new RedHeadDuck();
    @Test
    public void testPerformQuack() throws Exception {
        duck.performQuack();

        // should print Squeak behavior
        assertEquals("Squeeekk :(\n", outContent.toString());
    }
}
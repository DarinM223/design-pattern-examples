package com.d_m.strategy.test;

import com.d_m.strategy.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class DuckTest {
    protected final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    protected final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private Duck duck = new Duck();

    @org.junit.Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @org.junit.After
    public void tearDown() throws Exception {
        System.setOut(null);
        System.setOut(null);
    }

    @org.junit.Test
    public void testPerformQuack() throws Exception {
        duck.performQuack();

        // should print Quack behavior
        assertEquals("Quaaaack!!\n", outContent.toString());
    }

    @org.junit.Test
    public void testPerformFly() throws Exception {
        duck.performFly();

        // should print FlyWithWings behavior
        assertEquals("Flying with wings!\n", outContent.toString());
    }

    @org.junit.Test
    public void testSwim() throws Exception {
        duck.swim();

        // should print swim behavior
        assertEquals("Swimming!\n", outContent.toString());
    }

    @org.junit.Test
    public void testDisplay() throws Exception {
        duck.display();

        // should print display behavior
        assertEquals("I'm a duck!\n", outContent.toString());
    }
}
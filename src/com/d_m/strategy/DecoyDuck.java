package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class DecoyDuck extends Duck {
    public DecoyDuck() {
        super();
        // set fly and quack behaviors for decoy duck (no flying and no quacking)
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
    }
}

package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class NoFlyException extends Exception {
    public NoFlyException() {}

    public NoFlyException(String message) {
        super(message);
    }
}

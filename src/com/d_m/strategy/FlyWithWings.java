package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Flying with wings!");
    }
}

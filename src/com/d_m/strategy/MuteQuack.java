package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class MuteQuack implements QuackBehavior {
    @Override
    public void quack() {
        // Does nothing
    }
}

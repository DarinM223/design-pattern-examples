package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class NoQuackException extends Exception {
    public NoQuackException() {}

    public NoQuackException(String message) {
        super(message);
    }
}

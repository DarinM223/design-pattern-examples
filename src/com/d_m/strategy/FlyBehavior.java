package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public interface FlyBehavior {
    public void fly();
}

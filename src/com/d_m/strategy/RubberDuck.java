package com.d_m.strategy;

/**
 * Created by darin on 4/26/15.
 */
public class RubberDuck extends Duck {
    public RubberDuck() {
        super();

        // Rubber duck also cannot fly or quack
        this.setFlyBehavior(new FlyNoWay());
        this.setQuackBehavior(new MuteQuack());
    }
}
